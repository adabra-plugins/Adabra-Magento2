<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model;

use Adspray\Adabra\Api\Data\VFieldsInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class VFields extends AbstractModel implements VFieldsInterface
{
    protected $objectManager;
    protected $storeManager;

    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        ObjectManagerInterface $objectManager,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
    }

    protected function _construct()
    {
        $this->_init('Adspray\Adabra\Model\ResourceModel\VFields');
    }

    /**
     * Get VField code
     * @return string
     */
    public function getCode()
    {
        return $this->getData('code');
    }
    /**
     * Get VFields Value
     * @return string
     */
    public function geValue()
    {
        return $this->getData('value');
    }

    /**
     * Get VFields Mode
     * @return string
     */
    public function getMode()
    {
        return $this->getData('mode');
    }
}
