<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class VFields extends SourceAbstract implements SourceInterface, OptionSourceInterface
{
    const BRAND =           'brand';
    const MODEL =           'modello';
    const SHIPPING_PRICE =  'prezzo_spedizione';
    const INFO_PAYMENT =    'info_pagamento';
    const SHIPPING_TIME =   'tempo_spedizione';
    const SHIPPING_INFO =   'info_spedizione';
    const AVAILABLE_TO =    'fine_validita';
    const AVAILABLE_FROM =  'disponibile_dal';
    const PRIORITY =        'priorita';
    const CONDITION =       'condizione';
    const OVER_18 =         'f_peradulti';
    const GTIN =            'GTIN';
    const UPC =             'UPC';
    const EAN =             'EAN';
    const ISBN =            'ISBN';
    const ASIN =            'ASIN';
    const PZN =             'PZN';
    const CNET =            'CNET';
    const MUZEID =          'MUZEID';
    const MPN =             'MPN';

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::BRAND,            'label' => 'brand'],
            ['value' => self::MODEL,            'label' => 'modello'],
            ['value' => self::SHIPPING_PRICE,   'label' => 'prezzo_spedizione'],
            ['value' => self::INFO_PAYMENT,     'label' => 'info_pagamento'],
            ['value' => self::SHIPPING_TIME,    'label' => 'tempo_spedizione'],
            ['value' => self::SHIPPING_INFO,    'label' => 'info_spedizione'],
            ['value' => self::AVAILABLE_TO,     'label' => 'fine_validita'],
            ['value' => self::AVAILABLE_FROM,   'label' => 'disponibile_dal'],
            ['value' => self::PRIORITY,         'label' => 'priorita'],
            ['value' => self::CONDITION,        'label' => 'condizione'],
            ['value' => self::OVER_18,          'label' => 'f_peradulti'],
            ['value' => self::GTIN,             'label' => 'GTIN'],
            ['value' => self::UPC,              'label' => 'UPC'],
            ['value' => self::EAN,              'label' => 'EAN'],
            ['value' => self::ISBN,             'label' => 'ISBN'],
            ['value' => self::ASIN,             'label' => 'ASIN'],
            ['value' => self::PZN,              'label' => 'PZN'],
            ['value' => self::CNET,             'label' => 'CNET'],
            ['value' => self::MUZEID,           'label' => 'MUZEID'],
            ['value' => self::MPN,              'label' => 'MPN'],
        ];
    }
}
