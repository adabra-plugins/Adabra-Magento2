<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
     * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model\Source;

class OrderDaysInterval extends SourceAbstract
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 60,
                'label' => __('60 days')
            ], [
                'value' => 90,
                'label' => __('90 days')
            ], [
                'value' => 120,
                'label' => __('120 days')
            ], [
                'value' => 180,
                'label' => __('180 days')
            ], [
                'value' => 365,
                'label' => __('365 days')
            ],
        ];
    }
}
