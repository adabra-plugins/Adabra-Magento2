<?php

namespace Adspray\Adabra\Model\Config;

use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\App\CacheInterface;

/**
 * UseCron
 */
class UseCron extends \Magento\Framework\App\Config\Value
{
    const ADABRA_FEED_RUN_CRON_CONF_PATH = 'crontab/adabra/jobs/adabra_run/schedule/cron_expr';
    const ADABRA_FEED_REBUILD_CRON_CONF_PATH = 'crontab/adabra/jobs/adabra_rebuild/schedule/cron_expr';

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $configValueFactory;

    /**
     * UseCron constructor.
     * @param Logger $logger
     * @param CacheInterface $cache
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Logger $logger,
        CacheInterface $cache,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->logger = $logger;
        $this->cache = $cache;
        $this->configValueFactory = $configValueFactory;

        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * In base al campo use_cron vengono attivati/disattivati i cron adabra_run e adabra_rebuild
     * La disattivazione come da documentazione avviene impostando la data del job ad una data impossibile (30/02)
     * Dopo l'aggiornamento viene resettato il campo cron_last_schedule_generate_at . $groupId
     * @return void
     */
    public function afterSave(){
        $useCron = $this->getValue();
        if ($this->isValueChanged() ||
            $this->configValueFactory->create()->load(self::ADABRA_FEED_RUN_CRON_CONF_PATH, 'path')->getValue() == null ||
            $this->configValueFactory->create()->load(self::ADABRA_FEED_REBUILD_CRON_CONF_PATH, 'path')->getValue() == null){
            $this->logger->debug('use_cron is changed or null values');
            $feedRebuildTimeArray = $this->getData('groups/general/fields/rebuild_time/value');
            $feedRebuildTime = $feedRebuildTimeArray[0];
            $feedRunCron = "0 0 30 2 *";
            $feedRebuildCron = "0 0 30 2 *";
            if ($useCron){
                $feedRunCron = "*/2 * * * *";
                $feedRebuildCron = "0 " . $feedRebuildTime . " * * *";
            }
            $this->configValueFactory->create()->load(self::ADABRA_FEED_RUN_CRON_CONF_PATH, 'path')
                ->setValue($feedRunCron)->setPath(self::ADABRA_FEED_RUN_CRON_CONF_PATH)->save();
            $this->configValueFactory->create()->load(self::ADABRA_FEED_REBUILD_CRON_CONF_PATH, 'path')
                ->setValue($feedRebuildCron)->setPath(self::ADABRA_FEED_REBUILD_CRON_CONF_PATH)->save();
            $groupId = 'adabra';
            $this->cache->save(0, 'cron_last_schedule_generate_at' . $groupId); // reset cron_last_schedule_generate_at force reload cron_schedule
        }
        return parent::afterSave();
    }
}