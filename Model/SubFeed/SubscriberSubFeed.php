<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model\SubFeed;

use Adspray\Adabra\Api\Data\SubFeedInterface;
use Adspray\Adabra\Helper\Data as DataHelper;
use Adspray\Adabra\Helper\Ftp as FtpHelper;
use Adspray\Adabra\Helper\Filesystem;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;
use Magento\Directory\Helper\Data as DirectoryHelperData;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;



class SubscriberSubFeed extends AbstractSubFeed implements SubFeedInterface
{
    protected $type = 'subscriber';
    protected $scope = 'website';
    protected $exportName = 'subscriber';

    protected $collectionFactory;
    protected $subscriber;
    protected $storeManagerInterface;


    /**
     * SubscriberSubFeed constructor.
     * @param File $file
     * @param Csv $csv
     * @param Filesystem $filesystem
     * @param DataHelper $dataHelper
     * @param FtpHelper $ftpHelper
     * @param DirectoryHelperData $directoryHelperData
     * @param DateTime $dateTime
     * @param CollectionFactory $collectionFactory
     * @param Subscriber $subscriber
     * @param StoreManagerInterface $storeManagerInterface
     */
    public function __construct(
        File $file,
        Csv $csv,
        Filesystem $filesystem,
        DataHelper $dataHelper,
        FtpHelper $ftpHelper,
        DirectoryHelperData $directoryHelperData,
        DateTime $dateTime,
        CollectionFactory $collectionFactory,
        Subscriber $subscriber,
        StoreManagerInterface $storeManagerInterface
    ) {
        parent::__construct($file, $csv, $filesystem, $dataHelper, $ftpHelper, $directoryHelperData, $dateTime);

        $this->collectionFactory = $collectionFactory;
        $this->subscriber = $subscriber;
        $this->storeManagerInterface = $storeManagerInterface;

    }

    /**
     * Get headers
     * @return array
     */
    protected function getHeaders()
    {
        return [
            'email',
            'f_ricevi_newsletter',
            'f_ricevi_newsletter_ts',
            'f_attivo',
            'liste_newsletter',
            'mantieni_liste_attuali',
            'unsubscribe_code'
        ];
    }

    /**
     * Prepare feed collection
     * @return void
     */
    protected function prepareCollection()
    {
        $this->collection = $this->collectionFactory->create();
        /*leggo tutte le store view associate al website*/
        $storeList = array();
        foreach ($this->getFeed()->getStore()->getWebsite()->getGroups() as $group) {
            $stores = $group->getStores();
            foreach ($stores as $store) {
                array_push($storeList,  $store->getStoreId());

            }
        }
        $this->collection = $this->collectionFactory->create()
            ->addFieldToFilter('store_id', array ('in' => implode(",",$storeList)));
        if($this->dataHelper->isIncrementalEnabled()) {
            $subscriberFeedLastUpdate = $this->getFeed()->getFeedLastUpdate($this->type);
            if(isset($subscriberFeedLastUpdate)) {
                $this->collection->addFieldToFilter('change_status_at', ['gt' => $subscriberFeedLastUpdate]);
            }
        }

    }


    /**
     * @param $entity
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getFeedRow($entity)
    {
        /** @var $subscriber Subscriber */
        $subscriber = $entity;
        $store = $this->storeManagerInterface->getStore($subscriber->getStoreId());
        $status = $subscriber->getSubscriberStatus();
        $f_ricevi_newsletter_ts = $subscriber->getChangeStatusAt();
        $unsubscribe_code = $subscriber->getUnsubscriptionLink();
        $f_ricevi_newsletter = 0;
        $f_attivo = 0;
        switch($status) {

            case 1:
                $f_ricevi_newsletter = 1;
                $f_attivo = 1;
                break;

            case 2:
                $f_ricevi_newsletter = 0;
                $f_attivo = 0;
                break;

            case 3:
                $f_ricevi_newsletter = 0;
                $f_attivo = 1;
                break;

            case 4:
                $f_ricevi_newsletter = 0;
                $f_attivo = 0;
                break;
        }
        return [[
            $subscriber->getEmail(),
            $this->toBoolean($f_ricevi_newsletter),
            $f_ricevi_newsletter_ts,
            $this->toBoolean($f_attivo),
            $store->getWebsite()->getCode()."_".$store->getCode(),
            $this->toBoolean(true),
            $unsubscribe_code

        ]];
    }
}
