<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model\SubFeed;

use Adspray\Adabra\Api\Data\SubFeedInterface;

use Adspray\Adabra\Helper\Data as DataHelper;
use Adspray\Adabra\Helper\Ftp as FtpHelper;
use Adspray\Adabra\Helper\Filesystem;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;
use Magento\Directory\Helper\Data as DirectoryHelperData;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;

class CategorySubFeed extends AbstractSubFeed implements SubFeedInterface
{
    const FAKE_CATEGORY_NAME = 'Root Category';
    const FAKE_CATEGORY_ID = 'none';

    protected $collectionFactory;

    protected $type = 'category';
    protected $exportName = 'category';
    protected $storeManagerInterface;

    public function __construct(
        File $file,
        Csv $csv,
        Filesystem $filesystem,
        DataHelper $dataHelper,
        FtpHelper $ftpHelper,
        DirectoryHelperData $directoryHelperData,
        CollectionFactory $collectionFactory,
        DateTime $dateTime,
        StoreManagerInterface $storeManagerInterface
    ) {
        parent::__construct($file, $csv, $filesystem, $dataHelper, $ftpHelper, $directoryHelperData, $dateTime);

        $this->collectionFactory = $collectionFactory;
        $this->storeManagerInterface = $storeManagerInterface;
    }

    /**
     * Get headers
     * @return array
     */
    protected function getHeaders()
    {
        return [
            'category_id',
            'parent_category_id',
            'category_name',
            'category_description',
            'active',
        ];
    }

    /**
     * Get virtual rows
     * @return array
     */
    protected function getVirtualRows()
    {
        $storeRootCategory = $this->getRootCategoryId(($this->getFeed()->getStore()->getId()));
        return [[
            $storeRootCategory,
            0,
            self::FAKE_CATEGORY_NAME,
            '',
            $this->toBoolean(false),
        ]];
    }

    /**
     * Prepare feed collection
     * @return void
     */
    protected function prepareCollection()
    {
        $storeRootCategory = $this->getRootCategoryId(($this->getFeed()->getStore()->getId()));
        $this->collection = $this->collectionFactory->create();
        $this->collection
            ->addAttributeToSelect('*')
            ->addUrlRewriteToResult()
            ->addAttributeToFilter('path', array('like' => "1/{$storeRootCategory}/%"))
            ->setStoreId($this->getFeed()->getStore()->getId());
    }

    /**
     * Get feed row for entity
     * @param $entity
     * @return array
     */
    protected function getFeedRow($entity)
    {
        /** @var $category Category */
        $category = $entity;

        return [[
            $category->getId(),
            $category->getParentId(),
            $category->getName(),
            $category->getData('description'),
            $this->toBoolean($category->getIsActive()),
        ]];
    }

    /**
     * @param $store
     * @return $rootCategoryId
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getRootCategoryId($store)
    {
        $rootCategoryId = $this->storeManagerInterface->getStore($store)->getRootCategoryId();
        return $rootCategoryId;
    }
}
