<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category   Adspray
 * @package    Adspray_Adabra
 * @copyright  Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model;

use Adspray\Adabra\Api\Data\VFieldsInterface;
use Adspray\Adabra\Api\VFieldsManagerInterface;
use Adspray\Adabra\Helper\Data as DataHelper;
use Adspray\Adabra\Helper\Filesystem;
use Adspray\Adabra\Model\ResourceModel\VFields\Collection;
use Adspray\Adabra\Model\ResourceModel\VFields\CollectionFactory as VFieldsCollectionFactory;

class VFieldsManager implements VFieldsManagerInterface
{
    protected $filesystem;
    protected $vfieldsCollectionFactory;
    protected $vfields;
    protected $dataHelper;

    protected $vfieldsCollection = null;

    public function __construct(
        Filesystem $filesystem,
        VFieldsCollectionFactory $vfieldsCollectionFactory,
        VFieldsInterface $vfields,
        DataHelper $dataHelper
    ) {
        $this->filesystem = $filesystem;
        $this->vfieldsCollectionFactory = $vfieldsCollectionFactory;
        $this->vfields = $vfields;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Check if vfield has a magento attribute mapped
     * @param $code
     * @return Collection
     */
    public function checkVFieldToExport($code)
    {

        $this->vfieldsCollection = $this->vfieldsCollectionFactory->create();
        $this->vfieldsCollection->filterToBuild($code);
        return $this->vfieldsCollection;
    }


}
