<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @copyright Copyright (c) 2017 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Model;

use Adspray\Adabra\Model\Tracking\Storage;
use Adspray\Adabra\Helper\Data as DataHelper;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Customer\Model\Data\Customer;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Quote\Model\Quote\Item;
use Magento\Sales\Model\Order;

class Tracking
{
    const TRACK_PAGE_VIEW = 'trkPageView';
    const TRACK_CATEGORY_VIEW = "trkCategoryView";
    const TRACK_PRODUCT_VIEW = 'trkProductView';
    const TRACK_TAGS_LIST = 'trkTagsList';
    const TRACK_ADD_TO_CART = 'trkProductBasketAdd';
    const TRACK_REMOVE_FROM_CART = 'trkProductBasketRemove';
    const TRACK_PRODUCT_SALE = 'trkProductSale';
    const TRACK_SEARCH = 'trkProductLocalSearch';
    const TRACK_USER_REGISTRATION = 'trkUserRegistration';

    protected $queue = [];
    protected $_subscriber;

    /**
     * @var Storage
     */
    private $storage;
    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var ProductRepositoryInterface
     */

    private $productRepository;

    /**
     * @var Grouped
     */

    private $grouped;

    public function __construct(
        Storage $storage,
        TimezoneInterface $timezone,
        Subscriber $subscriber,
        Grouped $grouped,
        ProductRepositoryInterface $productRepository
    ) {
        $this->storage = $storage;
        $this->timezone = $timezone;
        $this->_subscriber= $subscriber;
        $this->grouped = $grouped;
        $this->productRepository = $productRepository;
    }

    /**
     * @param $action
     * @param $params
     * @return $this
     */
    protected function add($action, $params)
    {
        $this->storage->addToQueue($action, $params);
        return $this;
    }

    /**
     * Convert value to currency
     * @param $val
     * @return string
     */
    protected function _toCurrency($val)
    {
        return number_format($val, 4, '.', '');
    }

    /**
     * @param ProductInterface $product
     */
    public function trackAddToCart(ProductInterface $product)
    {
        $typeCodeGrouped = \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE;
        if($product->getTypeId() === $typeCodeGrouped){
            $parentId = $product->getId();
            $childrenIds = $this->grouped->getChildrenIds($parentId);
            $typeId = \Magento\GroupedProduct\Model\ResourceModel\Product\Link::LINK_TYPE_GROUPED;
            $actions = [];
            foreach (array_values($childrenIds[$typeId]) as $id){
                $simpleProduct = $this->productRepository->getById($id);
                $productSku = $simpleProduct->getSku();
                //$this->add(static::TRACK_ADD_TO_CART, ['productId' => $productSku, 'cartId' => null, 'ts' => time()]);
                $actions[] = [
                    'productId' => $productSku,
                    'cartId' => null,
                    'ts' => time()
                ];
            }
            if ($actions) {
                $this->add(static::TRACK_ADD_TO_CART, $actions);
            }
        }
        else {
            $this->add(static::TRACK_ADD_TO_CART, ['productId' => $product->getData('sku'), 'cartId' => null, 'ts' => time()]);
        }
    }

    /**
     * @param Item $item
     */
    public function trackRemoveFromCart(Item $item)
    {
        $this->add(static::TRACK_REMOVE_FROM_CART, ['productId' => $item->getProduct()->getData('sku'), 'cartId' => null, 'ts' => time()]);
    }

    /**
     * @param Order $order
     */
    public function trackOrder(Order $order)
    {
        // counter per la prima riga dell'ordine in cui si riportano le spese di spedizione totali
        $rowsCount = 0;
        $now = $this->timezone->date();
        $actions = [];

        foreach ($order->getAllVisibleItems() as $item) {

            /**
             * @var Order\Item $item
            */

            $isFirstRow = ($rowsCount == 0);
            if ($item->getProduct()->getTypeId() != 'simple') {
                continue;
            }

            if (!is_null($item->getParentItem())) {
                $item = $item->getParentItem();
            }
            // leggo il valore dell'eventuale sconto a carrello (coupon o regola carrello)
            $discount = abs($item->getBaseDiscountAmount());
            $taxAmount= $item->getBaseTaxAmount();
            $hiddenTaxAmount = $item->getBaseHiddenTaxAmount();

            // ripartisco lo sconto a carrello su tutti gli item dell'ordine
            if ($item->getQtyOrdered() > 0) {
                $discount = $discount / $item->getQtyOrdered();
                $taxAmount = $item->getBaseTaxAmount() / $item->getQtyOrdered();
                $hiddenTaxAmount = $item->getBaseHiddenTaxAmount() / $item->getQtyOrdered();
            }

            $price = $this->_toCurrency($item->getBasePrice() - $discount);
            $priceInclTax = $this->_toCurrency($price + $taxAmount + $hiddenTaxAmount);

            $actions[] = [
                $order->getIncrementId(),
                $item->getProduct()->getData('sku'),
                $item->getQtyOrdered(),
                $order->getCouponCode(),
                $price,
                $priceInclTax,
                $isFirstRow ? $this->_toCurrency($order->getBaseShippingInclTax()) : '',
                $order->getBaseCurrencyCode(),
                $now->format('Y-m-d\TH:i:s')
            ];
            $rowsCount++;
        }

        if ($actions) {
            $this->add(static::TRACK_PRODUCT_SALE, $actions);
        }
    }

    public function trackNewCustomer(Customer $customer)
    {
        $checkSubscriber = $this->_subscriber->loadByCustomerId($customer->getId());
        $isSubscribed = $checkSubscriber->isSubscribed();
        $this->add(
            static::TRACK_USER_REGISTRATION,
            [
                "userId" => $customer->getId(),
                "email" => $customer->getEmail(),
                "nome" => $customer->getFirstname(),
                "cognome" => $customer->getLastname(),
                "newsletterSubscription" => $isSubscribed,
                "comunicazioniCommerciali" => $isSubscribed
            ]
        );
    }

    /**
     * @param bool $clear
     * @return array
     */
    public function get($clear = false)
    {
        return $this->storage->getQueue($clear);
    }

    /**
     * @return $this
     */
    public function flushQueue()
    {
        $this->storage->flushAll();

        return $this;
    }
}
