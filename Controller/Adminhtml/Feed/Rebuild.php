<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2019-01-31
 * Time: 18:06
 */

namespace Adspray\Adabra\Controller\Adminhtml\Feed;
use Adspray\Adabra\Api\FeedManagerInterface;
use \Magento\Framework\Message\ManagerInterface;


use \Magento\Framework\App\Action\Action as controllerAction;
class Rebuild extends controllerAction {

    /**
     * @var FeedManagerInterface
     */
    protected $feedManagerInterface;

    /**
     * @var
     */
    protected $messageManager;



    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        FeedManagerInterface $feedManagerInterface,
        ManagerInterface $messageManager
    ) {
        $this->feedManagerInterface = $feedManagerInterface;
        $this->messageManager = $messageManager;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Adspray_Adabra::adabra_feed_rebuild');
    }
    /**
     * Sets the content of the response
     */
    public function execute() {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->feedManagerInterface->rebuild();
        $this->messageManager->addSuccessMessage(__("Rebuild feed correctly done"));
        return $resultRedirect->setPath('*/*/');

    }
}
