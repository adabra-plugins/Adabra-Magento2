<?php

namespace Adspray\Adabra\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            // Get module table
            $tableName = $setup->getTable('adabra_feed');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data

                $columns = ['status_subscriber' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => false, 'after' => 'status_customer', 'comment' => 'Status subscriber']];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {

            // Get module table
            $tableName = $setup->getTable('adabra_feed');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data

                $columns = ['category_feed_last_update' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 'nullable' => true, 'comment' => 'Category Feed last update']];
                $columns = array_merge($columns, ['product_feed_last_update' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 'nullable' => true, 'comment' => 'Product Feed last update']]);
                $columns = array_merge($columns, ['order_feed_last_update' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 'nullable' => true, 'comment' => 'Order Feed last update']]);
                $columns = array_merge($columns, ['customer_feed_last_update' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 'nullable' => true, 'comment' => 'Customer Feed last update']]);
                $columns = array_merge($columns, ['subscriber_feed_last_update' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 'nullable' => true, 'comment' => 'Subscriber Feed last update']]);

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {

            // Get module table
            $tableName = $setup->getTable('adabra_feed');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data

                $columns = [
                    'adabra_site_id' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => false, 'after' => 'store_id', 'comment' => 'ID portale Adabra'],
                    'adabra_catalog_id' => ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'nullable' => false, 'after' => 'adabra_site_id', 'comment' => 'ID catalogo Adabra'],

                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        $setup->endSetup();
    }
}
