<?php
/**
 * IDEALIAGroup srl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@idealiagroup.com so we can send you a copy immediately.
 *
 * @category  Adspray
 * @package   Adspray_Adabra
 * @copyright Copyright (c) 2016 IDEALIAGroup srl (http://www.idealiagroup.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Adspray\Adabra\Helper;

use Adspray\Adabra\Api\Data\FeedInterface;
use Adspray\Adabra\Logger\Logger;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Eav\Model\Config;
use Magento\Catalog\Model\ResourceModel\Product;
use \Magento\Framework\App\State;

class Data
{
    const XML_PATH_BATCH_SIZE = 'adabra_feed/batch_size';
    const XML_PATH_ORDER_STATES = 'adabra_feed/order/states';
    const XML_PATH_ORDER_DAYS_INTERVAL = 'adabra_feed/order/days_interval';

    const XML_PATH_GENERAL_CRON = 'adabra_feed/general/use_cron';
    const XML_PATH_GENERAL_COMPRESS = 'adabra_feed/general/compress';
    const XML_PATH_GENERAL_REBUILD_TIME = 'adabra_feed/general/rebuild_time';
    const XML_PATH_GENERAL_INCREMENTAL_ENABLED = 'adabra_feed/general/incremental_enabled';
    const XML_PATH_GENERAL_API_NOTIFY = 'adabra_feed/general/api_notify';
    const XML_PATH_GENERAL_API_STAGING_MODE = 'adabra_feed/general/api_staging_mode';
    const XML_PATH_GENERAL_ENABLE_LOGGING_MODE = 'adabra_feed/general/logging_mode_enabled';

    const XML_PATH_HTTP_ENABLED = 'adabra_feed/http/enabled';
    const XML_PATH_HTTP_USER = 'adabra_feed/http/user';
    const XML_PATH_HTTP_PASS = 'adabra_feed/http/pass';

    const XML_PATH_FTP_ENABLED = 'adabra_feed/ftp/enabled';
    const XML_PATH_FTP_USER = 'adabra_feed/ftp/user';
    const XML_PATH_FTP_PASS = 'adabra_feed/ftp/pass';
    const XML_PATH_FTP_HOST = 'adabra_feed/ftp/host';
    const XML_PATH_FTP_PATH = 'adabra_feed/ftp/path';
    const XML_PATH_FTP_PORT = 'adabra_feed/ftp/port';
    const XML_PATH_FTP_SSL = 'adabra_feed/ftp/ssl';
    const XML_PATH_FTP_PASSIVE = 'adabra_feed/ftp/passive';

    const XML_PATH_FTP_SSH_ENABLED = 'adabra_feed/ftp_ssh/enabled';
    const XML_PATH_FTP_SSH_USER = 'adabra_feed/ftp_ssh/user';
    const XML_PATH_FTP_SSH_KEY = 'adabra_feed/ftp_ssh/public_key';
    const XML_PATH_FTP_SSH_HOST = 'adabra_feed/ftp_ssh/host';
    const XML_PATH_FTP_SSH_PATH = 'adabra_feed/ftp_ssh/path';
    const XML_PATH_FTP_SSH_PORT = 'adabra_feed/ftp_ssh/port';

    const XML_PATH_ORDER_FILTER_ENABLE = 'adabra_feed/order/exclude_order';
    const XML_PATH_ORDER_FILTER_LIST = 'adabra_feed/order/filter_list';

    const XML_PATH_PRODUCT_CUSTOM_SHORT_DESCRIPTION_ENABLE = 'adabra_feed/products/enable_custom_short_description';
    const XML_PATH_PRODUCT_CUSTOM_SHORT_DESCRIPTION = 'adabra_feed/products/custom_short_description';

    const XML_PATH_CUSTOMER_EXPORT = 'adabra_feed/customer/export';
    const XML_PATH_CUSTOMER_ADDRESS_TYPE = 'adabra_feed/customer/address_type';
    const XML_PATH_NEWSLETTER_EXPORT = 'adabra_feed/newsletter/export';

    const XML_PATH_TRACKING_ENABLED = 'adabra_tracking/general/enabled';
    const XML_PATH_TRACKING_URL = 'adabra_tracking/general/tracking_url';
    const XML_PATH_SITE_ID = 'adabra_tracking/general/site_id';
    const XML_PATH_CATALOG_ID = 'adabra_tracking/general/catalog_id';

    const XML_PATH_FEED_TAGS_LIST = 'adabra_feed/tags/tags_list';
    const XML_PATH_TRACKING_TAGS_LIST = 'adabra_tracking/tags/tags_list';
    const XML_PATH_TRACKING_DISABLE_TRACKING_CART = 'adabra_tracking/disable_tracking_cart_actions';

    /* parametri per chiamate API */

    const API_MAGENTO_SIGNATURE_KEY = 'Twdw%!wH$J=m@q@J?bVGR4_g=ZPCruGq^8h@VLnE!qn#tm^G&z2&u*w2QMznU8!W';
    const RELATIVE_ENDPOINT = 'api/magento/import/';
    const BASE_URL_STAGING = 'http://staging.marketingspray.com/';
    const BASE_URL_PRODUCTION = 'https://my.adabra.com/';

    const CATALOG_ACTION = 'catalog';
    const CUSTOMER_ACTION = 'customer';
    const ORDER_ACTION = 'order';
    const NEWSLETTER_ACTION = 'newsletter';
    const AREA_CODE = \Magento\Framework\App\Area::AREA_ADMINHTML;


    protected $scopeConfig;
    protected $feed;
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;
    protected $eavConfig;
    protected $resourceProduct;
    protected $state;
    protected $_logger;


    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        AttributeRepositoryInterface $attributeRepository,
        FeedInterface $feed,
        \Magento\Framework\HTTP\Client\Curl $curl,
        Config $eavConfig,
        Product $resourceProduct,
        State $state,
        Logger $logger

    ) {
        $this->scopeConfig = $scopeConfig;
        $this->attributeRepository = $attributeRepository;
        $this->feed = $feed;
        $this->eavConfig = $eavConfig;
        $this->resourceProduct = $resourceProduct;
        $this->state = $state;

        $this->curlClient = $curl;
        $this->logger = $logger;

    }

    /**
     * Get products batch size
     *
     * @return bool
     */
    public function getCompress()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_COMPRESS);
    }

    /**
     * Get batch size for a sub-feed
     *
     * @param  $subFeedType
     * @return int
     */
    public function getBatchSize($subFeedType)
    {
        $this->feed->checkSubFeedType($subFeedType);
        $xmlPath = static::XML_PATH_BATCH_SIZE.'/'.$subFeedType;

        return $this->scopeConfig->getValue($xmlPath);
    }

    /**
     * Return true if batch is enabled for a sub-feed
     *
     * @param  $subFeedType
     * @return bool
     */
    public function isBatchEnabled($subFeedType)
    {
        return $this->getBatchSize($subFeedType) > 0;
    }

    /**
     * Get order states for export
     *
     * @return array
     */
    public function getOrderStates()
    {
        return explode(',', $this->scopeConfig->getValue(static::XML_PATH_ORDER_STATES));
    }

    /**
     * Get days interval for order export
     *
     * @return string
     */
    public function getDaysInterval()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_ORDER_DAYS_INTERVAL);
    }

    /**
     * Is http download enabled
     *
     * @return bool
     */
    public function isHttpEnabled()
    {
        if (!$this->scopeConfig->getValue(static::XML_PATH_HTTP_ENABLED)) {
            return false;
        }

        return ($this->getHttpAuthUser() && $this->getHttpAuthPassword());
    }

    /**
     * Get auth username
     *
     * @return string
     */
    public function getHttpAuthUser()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_HTTP_USER) ? $this->scopeConfig->getValue(static::XML_PATH_TRACKING_TAGS_LIST) : "");
    }

    /**
     * Get auth password
     *
     * @return string
     */
    public function getHttpAuthPassword()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_HTTP_PASS) ? $this->scopeConfig->getValue(static::XML_PATH_HTTP_PASS) : "");
    }

    /**
     * Get FTP user
     *
     * @return string
     */
    public function getFtpUser()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_USER) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_USER) : "");
    }

    /**
     * Get FTP pass
     *
     * @return string
     */
    public function getFtpPass()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_PASS) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_PASS) : "");
    }

    /**
     * Get FTP path
     *
     * @return string
     */
    public function getFtpPath()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_PATH) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_PATH) : "");
    }

    /**
     * Get FTP host
     *
     * @return string
     */
    public function getFtpHost()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_HOST) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_HOST) : "");
    }

    /**
     * Get FTP port
     *
     * @return int
     */
    public function getFtpPort()
    {
        return intval(trim($this->scopeConfig->getValue(static::XML_PATH_FTP_PORT) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_PORT) : 21));
    }

    /**
     * Get SSL mode
     *
     * @return bool
     */
    public function getFtpSsl()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_FTP_SSL);
    }

    /**
     * Get passive mode
     *
     * @return bool
     */
    public function getFtpPassive()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_FTP_PASSIVE);
    }

    /**
     * Is ftp enabled
     *
     * @return bool
     */
    public function isFtpEnabled()
    {
        if (!$this->scopeConfig->getValue(static::XML_PATH_FTP_ENABLED)) {
            return false;
        }

        return ($this->getFtpUser() && $this->getFtpPass() && $this->getFtpHost());
    }

    /**
     * Is ftp ssh enabled
     *
     * @return bool
     */
    public function isFtpSshEnabled()
    {
        if (!$this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_ENABLED)) {
            return false;
        }

        return ($this->getSshFtpUser() && $this->getSshFtpKey() && $this->getSshFtpHost());
    }

    /**
     * Get FTP ssh user
     *
     * @return string
     */
    public function getSshFtpUser()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_USER) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_USER) : "");
    }

    /**
     * Get FTP ssh key
     *
     * @return string
     */
    public function getSshFtpKey()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_KEY) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_KEY) : "");
    }

    /**
     * Get FTP ssh path
     *
     * @return string
     */
    public function getSshFtpPath()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_PATH) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_PATH) : "");
    }

    /**
     * Get FTP ssh host
     *
     * @return string
     */
    public function getSshFtpHost()
    {
        return trim($this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_HOST) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_HOST) : "");
    }

    /**
     * Get FTP ssh port
     *
     * @return int
     */
    public function getSshFtpPort()
    {
        return intval(trim($this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_PORT) ? $this->scopeConfig->getValue(static::XML_PATH_FTP_SSH_PORT) : 21));
    }

    /**
     * Is order filter enabled
     *
     * @return bool
     */
    public function isOrderFilterEnable()
    {
        return boolval(trim($this->scopeConfig->getValue(static::XML_PATH_ORDER_FILTER_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES) ? $this->scopeConfig->getValue(static::XML_PATH_ORDER_FILTER_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES) : false));
    }

    /**
     * get Order Exclusion Filter List of Expression
     *
     * @return string
     */
    public function getOrderFilterList()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_ORDER_FILTER_LIST, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * Is Custom product's short description enabled
     *
     * @return bool
     */
    public function isProductFeedCustomShortDescriptionEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_PRODUCT_CUSTOM_SHORT_DESCRIPTION_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * get Custom product's short description
     *
     * @return string
     */
    public function getProductFeedCustomShortDescription()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_PRODUCT_CUSTOM_SHORT_DESCRIPTION, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * Is Customer export enabled
     * @return bool
     */
    public function isCustomerExportEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_CUSTOMER_EXPORT, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * Get customer address type to export
     * @return string
     */
    public function getCustomerAddressType()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_CUSTOMER_ADDRESS_TYPE);

    }

    /**
     * Is Newsletter export enabled
     * @return bool
     */
    public function isNewsletterExportEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_NEWSLETTER_EXPORT, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * Tracking enabled
     *
     * @return bool
     */
    public function getTrackingEnabled()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_TRACKING_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

    /**
     * @return null|string
     */
    public function getTrackingUrl()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_TRACKING_URL, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * @return null|string
     */
    public function getSiteId()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_SITE_ID, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * @return null|string
     */
    public function getCatalogId()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_CATALOG_ID,\Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }


    /**
     * @return array
     */
    public function getFeedTagsList()
    {
        $tags = trim($this->scopeConfig->getValue(static::XML_PATH_FEED_TAGS_LIST) ? $this->scopeConfig->getValue(static::XML_PATH_FEED_TAGS_LIST) : "");
        return !empty($tags) ? explode(PHP_EOL, $tags) : array();
    }

    /**
     * @return array
     */
    public function getTrackingTagsList()
    {
        $tags = trim($this->scopeConfig->getValue(static::XML_PATH_TRACKING_TAGS_LIST) ? $this->scopeConfig->getValue(static::XML_PATH_TRACKING_TAGS_LIST) : "");
        return !empty($tags) ? explode(PHP_EOL, $tags) : array();
    }

    /**
     * Check if cart action tracking are disabled
     *
     * @return bool
     */
    public function isTrackingCartDisable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_TRACKING_DISABLE_TRACKING_CART, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);

    }

    /**
     * Recursively implodes an array with optional key inclusion
     *
     * Example of $include_keys output: key, value, key, value, key, value
     *
     * @access  public
     * @param   array   $array         multi-dimensional array to recursively implode
     * @param   string  $glue          value that glues elements together
     * @param   bool    $include_keys  include keys before their values
     * @param   bool    $trim_all      trim ALL whitespace from string
     * @return  string  imploded array
     */
    public function recursive_implode(array $array, $glue = ',', $include_keys = false, $trim_all = true)
    {
        $glued_string = '';
        // Recursively iterates array and adds key/value to glued string
        array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string)
        {
            $include_keys and $glued_string .= $key.$glue;
            $glued_string .= $value.$glue;
        });
        // Removes last $glue from string
        strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));
        // Trim ALL whitespace
        //$trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);
        $glued_string = trim($glued_string);
        return (string) $glued_string;
    }

    /**
     * Get custom tags list
     * @access public
     * @param Product $product
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomTagsList($product)
    {
        $tagList = [];
        $tagsListArray = $this->getFeedTagsList();
        foreach ($tagsListArray as $tag) {

            $tag = trim($tag);
            $tagToExport = $this->getTagToExport($product->getId(), $tag);
            if (!empty($tagToExport)) {
                array_push($tagList, $tagToExport);
            }
        }
        return $tagList;
    }

    /**
     * Is Api notify enabled
     * @return bool
     */
    public function isApiNotifyEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_API_NOTIFY,\Magento\Store\Model\ScopeInterface::SCOPE_STORES);

    }

    /**
     * Is Api Staging Mode enabled
     * @return bool
     */
    public function isApiStagingModeEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_API_STAGING_MODE,\Magento\Store\Model\ScopeInterface::SCOPE_STORES);

    }

    /**
     * Is Logging  Mode enabled
     * @return bool
     */
    public function isLoggingModeEnable()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_ENABLE_LOGGING_MODE,\Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

    /**
     * Send Api call to Adabra to notify ending of feed generation
     * @param $feedType
     * @param null $idPortale
     * @param null $idCatalogo
     * @return curl result
     */

    public function sendApiCallToAdabra($feedType, $idPortale = null, $idCatalogo = null)
    {
        $action = '';
        switch ($feedType) {
            case 'product':
                $action = self::CATALOG_ACTION;
                break;
            case 'customer':
                $action = self::CUSTOMER_ACTION;
                break;
            case 'order':
                $action = self::ORDER_ACTION;
                break;
            case 'subscriber':
                $action = self::NEWSLETTER_ACTION;
                break;
        }
        $sendingData = [];
        $tmpHash = '';
        $sendingData['idCatalogo'] = $idCatalogo;
        $sendingData['idPortale'] = $idPortale;

        ksort($sendingData);

        foreach($sendingData as $key => $value) {
            $tmpHash .= $key . json_encode($value);
        }
        $magentoSignature  = md5($tmpHash . self::API_MAGENTO_SIGNATURE_KEY);
        $sendingData['magentoSignature'] = $magentoSignature;

        if($this->isApiStagingModeEnable()){
            $baseUrl = self::BASE_URL_STAGING;
        }
        else {
            $baseUrl = self::BASE_URL_PRODUCTION;
        }
        $url = $baseUrl.self::RELATIVE_ENDPOINT.$action;
        $this->curlClient->post($url, json_encode($sendingData,true));
        $this->curlClient->addHeader("Content-Type", "application/json");
        $this->curlClient->setOption("CURLOPT_RETURNTRANSFER",true);

        $response = $this->curlClient->getBody();
        return json_decode($response);

    }


    /**
     * @param $entityId
     * @param $attribute
     * @param null $store
     * @return mixed|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTagToExport($entityId, $attribute, $store=null) {
        if (!$store) {
            $store = $this->feed->getStore();
        }
        $attributeModel = $this->eavConfig->getAttribute('catalog_product', $attribute);
        $attributeType = $attributeModel->getFrontendInput();
        $label =  $attributeModel->getStoreLabel($store->getId());
        $value = $this->resourceProduct->getAttributeRawValue($entityId, $attribute, $store);
        if (!empty($value)) {
            $tag = '';
            switch ($attributeType) {
                /* per gli attributi boolean si riporta solamente la label in caso l'attributo sia true */
                case "boolean":
                    if ($value == 1) {
                        $tag = $attributeModel->getStoreLabel($store->getId());
                    }
                    break;
                case "text":
                    $tag = $label.":".$value;
                    break;

                case "textarea":
                    $tag = $label.":".$value;
                    break;

                case "select":
                    $tag = $label.":".$this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value);
                    break;

                case "multiselect":
                    if (is_array($this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value))) {
                        $attributeList = $label . ":" . implode(",", $this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value));
                        $tag = $attributeList;
                    } else
                        $tag = $label.":".$this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value);
                    break;
            }
            return $tag = str_replace('|', '', $tag);
        }

        return null;
    }

    /**
     * @param $entityId
     * @param $attribute
     * @param null $store
     * @return mixed|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAttributeToExport($entityId, $attribute, $store=null) {
        if (!$store) {
            $store = $this->feed->getStore();
        }
        $attributeModel = $this->eavConfig->getAttribute('catalog_product', $attribute);
        $attributeType = $attributeModel->getFrontendInput();
        $value = $this->resourceProduct->getAttributeRawValue($entityId, $attribute, $store);
        if (!empty($value)) {
            $attributeToExport = '';
            switch ($attributeType) {
                /* per gli attributi boolean si riporta solamente la label in caso l'attributo sia true */
                case "boolean":
                    if ($value == 1) {
                        $attributeToExport = $value;
                    }
                    break;
                case "textarea":
                case "text":
                    $attributeToExport = $value;
                    break;

                case "select":
                    $attributeToExport = $this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value);
                    break;

                case "multiselect":
                    if (is_array($this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value))) {
                        $attributeList = implode(",", $this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value));
                        $attributeToExport = $attributeList;
                    } else {
                        $attributeToExport = $this->resourceProduct->getAttribute($attribute)->getSource()->getOptionText($value);
                    }
                    break;
            }
            return $attributeToExport;
        }

        return null;
    }

    /**
     * Use Magento cron is enabled
     * @return bool
     */
    public function useCronIsEnabled()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_CRON, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * Get feed rebuild time
     * @return mixed
     */
    public function getFeedRebuildTime()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_GENERAL_REBUILD_TIME, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * Is Incremental feed export enabled
     * @return bool
     */
    public function isIncrementalEnabled()
    {
        return (bool) $this->scopeConfig->getValue(static::XML_PATH_GENERAL_INCREMENTAL_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isAdmin()
    {
        $areaCode = $this->state->getAreaCode();

        if ($areaCode == self::AREA_CODE) {
            return true;
        } else {
            return false;
        }
    }

    public function logError($e){
        if($this->isLoggingModeEnable()) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }
    }

    /**
     * @param $string
     * @param string $escapeChar
     * @return false|string
     */
    public function sanitizeString($string, $escapeChar='\\') {
        if ($string && mb_substr($string ?: "", -1) === $escapeChar) {
            $string = substr($string, 0, -1);
        }
        return $string;
    }
}