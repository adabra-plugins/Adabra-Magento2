<?php

namespace Adspray\Adabra\Logger;
use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::WARNING;

    protected $fileName = '/var/log/adabra.log';
}